#include "Timer.h"

#include <iostream>
#include <SDL.h>


Timer::Timer(int tps)
{
	prevTime = 0;
	currentTime = 0;
	deltaTime = 0;
	frameTime = 0;
	TicksPerSecond = (float)tps;
	frameInterval = 1.0f / tps;
}

void Timer::SetTicksPerSecond(int tps)
{
	TicksPerSecond = (float)tps;
	frameInterval = 1.0f / tps;
}

void Timer::Update()
{
	prevTime = currentTime;
	currentTime = SDL_GetTicks();
	deltaTime = (currentTime - prevTime) / 1000.0f;
	frameTime += deltaTime;
}

bool Timer::HasTicked()
{
	if (frameTime >= frameInterval)
	{
		frameTime = 0;
		return true;
	}

	return false;
}

void Timer::BeginCountdown(int seconds)
{
	countdownSeconds = seconds;

	countdownStart = SDL_GetTicks();
}

bool Timer::CountdownEnded()
{
	if (SDL_GetTicks() >= (Uint32)(countdownStart + (countdownSeconds * 1000)))
		return true;

	return false;
}
