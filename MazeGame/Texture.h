#pragma once
/*
*
* Code was originally from Lazy Foo' Productions (http://lazyfoo.net/)
*
*/

#include <string>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "GFXData.h"

//Texture wrapper class
class Texture
{
public:
	Texture();
	~Texture();
	bool LoadFromFile(GFXData &myGFXData, std::string path);
	bool LoadFromRenderedText(TTF_Font *font, GFXData &myGFXData, std::string textureText, SDL_Color textColor);
	void Free();
	void Render(GFXData &myGFXData, int x, int y);
	int GetWidth();
	int getHeight();
	// TODO There's a render function here, might be better to use that then render outside
	SDL_Texture* getTexture();

private:
	int mWidth;
	int mHeight;
	SDL_Texture* mTexture;
};
