#include "Ghost.h"


Ghost::Ghost(SDL_Texture* texture, int ghostNum) : GameObject(texture)
{
	timer = new Timer(2);

	src.w = 16;
	src.h = 16;

	switch (ghostNum) {
		case 1:
			src.x = 228;
			src.y = 64;
			delay = 1;
			break;
		case 2:
			src.x = 228;
			src.y = 80;
			delay = 2;
			break;
		case 3:
			src.x = 228;
			src.y = 96;
			delay = 3;
			break;
		case 4:
			src.x = 228;
			src.y = 112;
			delay = 4;
			break;
	}


}

Ghost::~Ghost()
{
	delete timer;
}

void Ghost::Move(Maze &maze, std::list<Node*>* targetPath)
{
	timer->Update();
	if (timer->HasTicked()) {
		if (delay > 0) {
			delay--;
		}
		else {
			if (!targetPath->empty()) {
				Node* n = targetPath->front();
				x = n->GetX();
				y = n->GetY();
			}
		}
	}
}

void Ghost::Animate()
{
	timer->Update();
	if (timer->HasTicked()) {
		animFrame = (animFrame + 1) % 2;
		src.x = 228 + (16 * animFrame);

	}

}
