#pragma once

#include <string>
#include "structs.h"

// Init and shutdown SDL
bool InitGFX(GFXData &myGFXData);											// Starts up SDL and creates window
void ShutdownGFX(GFXData &myGFXData);										// Frees media and shuts down SDL
