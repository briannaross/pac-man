#include "GameObject.h"

SDL_Renderer* GameObject::renderer = nullptr;

GameObject::GameObject(SDL_Texture* texture)
{
	this->texture = texture;

	vector.x = 0;
	vector.y = 0;

	dst.w = 16;
	dst.h = 16;

	lerping = false;
}


GameObject::~GameObject()
{
}


void GameObject::SetPosition(int x, int y)
{
	this->x = x;
	this->y = y;
}

int GameObject::GetX()
{
	return x;
}

int GameObject::GetY()
{
	return y;
}

void GameObject::Render(SDL_Texture* t)
{
	dst.x = (int)x * 16;
	dst.y = (int)y * 16;
	SDL_RenderCopy(renderer, t, &src, &dst);
}

void GameObject::SetRenderer(SDL_Renderer* r)
{
	renderer = r;
}
