#pragma once
#include <list>
#include <SDL.h>
#include "GameObject.h"
#include "Maze.h"
#include "Timer.h"

class Ghost :
	public GameObject
{
public:
	Ghost(SDL_Texture* texture, int ghostNum);
	~Ghost();
	void Move(Maze &maze, std::list<Node*>* targetPath);
	void Animate();
	void WhereTheHellAmI()
	{
		std::cout << "Ghost: " << x << ", " << y << std::endl;
	}

	void Reset() {}

private:
	Timer *timer;
	int delay;
	int animFrame;
};

