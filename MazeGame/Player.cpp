#include "Player.h"



Player::Player(SDL_Texture* texture) : GameObject(texture)
{
	src.x = 244;
	src.y = 0;
	src.h = 16;
	src.w = 16;

	score = 0;

	timer = new Timer(4);
}


Player::~Player()
{
	delete timer;
}

void Player::MoveUp(Maze &maze)
{
	if (y > 0.0f) {
		if (maze.GetNode((int)x, (int)y - 1)->IsWalkable()) {
			vector.x = 0;
			vector.y = -1;
			Move(vector);
		}
	}

	src.y = 32;
}

void Player::MoveDown(Maze &maze)
{
	if (y < MAZE_HEIGHT - 1) {
		if (maze.GetNode((int)x, (int)y + 1)->IsWalkable()) {
			vector.x = 0;
			vector.y = 1;
			Move(vector);
		}
	}

	src.y = 48;
}

void Player::MoveLeft(Maze &maze)
{
	// Pac-man can move from the left exit to the right exit
	if (x > 0.0f) {
		if (maze.GetNode((int)x - 1, (int)y)->IsWalkable()) {
			vector.x = -1;
			vector.y = 0;
			Move(vector);
		}
	}
	else if (x == 0 && y == 14.0f) {
		vector.x = -1;
		vector.y = 0;
		Move(vector);
	}

	src.y = 16;
}

void Player::MoveRight(Maze &maze)
{
	// Pac-man can move from the right exit to the left exit
	if (x < MAZE_WIDTH - 1) {
		if (maze.GetNode((int)x + 1, (int)y)->IsWalkable()) {
			vector.x = 1;
			vector.y = 0;
			Move(vector);
		}
	}
	else if (x == 27 && y == 14.0f)
	{
		vector.x = 1;
		vector.y = 0;
		Move(vector);
	}

	src.y = 0;
}

void Player::Animate()
{
	timer->Update();
	if (timer->HasTicked()) {
		animFrame = (animFrame + 1) % 2;
		src.x = 228 + (16 * animFrame);
	}
}

bool Player::EatPill(SDL_Point &pillLoc)
{
	if (GetX() == pillLoc.x && GetY() == pillLoc.y) {
		score++;
		return true;
	}

	return false;

}

void Player::Move(SDL_Point &vector)
{
	x = x + vector.x;
	if (x < 0) {
		x = (MAZE_WIDTH - 1);
	}
	else if (x >= MAZE_WIDTH) {
		x = 0;
	}

	y = y + vector.y;
}

void Player::WhereTheHellAmI()
{
	std::cout << "Player: " << x << ", " << y << std::endl;
}
