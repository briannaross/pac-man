#pragma once

#include <SDL.h>
#include "Window.h"

struct GFXData
{
	LF_Window window;
	SDL_Renderer* renderer = NULL;
};
