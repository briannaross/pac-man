#pragma once
#include <SDL.h>
#include "GameObject.h"

class Pill :
	public GameObject
{
public:
	Pill(SDL_Texture* texture, int x, int y);
	~Pill();

	void WhereTheHellAmI() {};
	void Reset() {};

};

