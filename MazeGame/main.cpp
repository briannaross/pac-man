#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <string>
#include <vector>
#include <list>
#include "SDLWrapper.h"
#include "GFXData.h"
#include "Structs.h"
#include "Texture.h"
#include "Maze.h"
#include "Player.h"
#include "Ghost.h"
#include "Pill.h"
#include "Timer.h"

typedef std::list<Node*> NodeList;

bool InitResources(GFXData &gfxData, Resources &resources);
NodeList* FindShortestPath(Maze &maze, Node *start, Node *target);
Node* LowestOpenFCostNode(NodeList openList);
Node* GetNode(NodeList list, Node* node);
bool FindNode(NodeList list, Node* node);
int Heuristic(Node* start, Node* target);
NodeList* ConstructPath(Node* target);
NodeList GetNeighbours(Node* current, Maze& maze);


int main(int argc, char* args[])
{
	GFXData gfxData;
	std::list<GameObject *> gameObjects;

	// Start up GFX and create window
	if (InitGFX(gfxData))
	{
		/**********************/
		/*** INITIALISATION ***/
		/**********************/

		// Set up resources. If any load fails exit program
		Resources resources;
		if (!InitResources(gfxData, resources))
			exit(1);

		Maze maze(gfxData.renderer);

		// SDL event handler
		SDL_Event e;
		bool playGame = true;

		GameObject::SetRenderer(gfxData.renderer);

		// Create the pills (not the power pills)
		for (int i = 0; i < MAZE_WIDTH; i++) {
			for (int j = 0; j < MAZE_HEIGHT; j++) {
				Node* n = maze.GetNode(i, j);
				if ((n->IsWalkable()) && !(n->IsGhostSpawnPoint())) {
					Pill *pill = new Pill(resources.pill.getTexture(), i, j);
					gameObjects.push_back(pill);
				}
			}
		}

		// Create the four ghosts (Inky, Pinky, Blinky and Clyde)
		for (int i = 1; i <= 4; i++) {
			Ghost* ghost = new Ghost(resources.spriteSheet.getTexture(), i);
			ghost->SetPosition(11 + i, 14);
			gameObjects.push_back(ghost);
		}

		// Create the player
		Player* player = new Player(resources.spriteSheet.getTexture());
		player->SetPosition(1, 1);
		gameObjects.push_back(player);


		// TODO Set these to the locations of player, enemies, whatever required for the task

		// Set variables for pathfinding
		Node *source; // A given ghost
		Node *target; // Player (yum, yum!)
		NodeList *shortestPath = new NodeList();


		Timer fps(60);

		while (playGame)
		{
			fps.Update();
			if (fps.HasTicked()) {
				while (SDL_PollEvent(&e) != 0) {
					// User requests quit ('X' in top-right of window on Windows)
					if (e.type == SDL_QUIT)
						playGame = false;
					if (e.type == SDL_KEYDOWN) {
						switch (e.key.keysym.sym) {
							case SDLK_UP:
								player->MoveUp(maze);
								break;
							case SDLK_DOWN:
								player->MoveDown(maze);
								break;
							case SDLK_LEFT:
								player->MoveLeft(maze);
								break;
							case SDLK_RIGHT:
								player->MoveRight(maze);
								break;
						}
					}
				}
				
				target = maze.GetNode((int)player->GetX(), (int)player->GetY());

				Pill* deletion = nullptr;
				for (auto gameObject : gameObjects) {
					Ghost* g = dynamic_cast<Ghost*>(gameObject);
					Pill* p = dynamic_cast<Pill*>(gameObject);

					if (g != nullptr) {
						source = maze.GetNode((int)g->GetX(), (int)g->GetY());
						shortestPath = FindShortestPath(maze, source, target);
						g->Move(maze, shortestPath);
						g->Animate();

						for (auto pathNode : *shortestPath) {
							delete pathNode;
						}
						shortestPath->clear();
					}

					if (p != nullptr) {
						if (player->EatPill(SDL_Point{ p->GetX(), p->GetY() })) {
							deletion = p;
						}
					}

					player->Animate();

				}

				if (deletion != nullptr)
					gameObjects.remove(deletion);


				/*******************/
				/**** RENDERING ****/
				/*******************/

				// Clear the window and make it all red
				// TODO Check that it really is rendering in red!
				SDL_RenderClear(gfxData.renderer);

				// Render everything on the screen
				maze.Render(resources.spriteSheet.getTexture());


				for (auto gameObject : gameObjects) {
					Pill* p = dynamic_cast<Pill*>(gameObject);
					if (p != nullptr) {
						p->Render(resources.pill.getTexture());
					}
					else {
						gameObject->Render(resources.spriteSheet.getTexture());
					}
				}

				// Render the shortest path (used for debugging [or to show how cool this is] only!)
				//SDL_Rect dst;
				//dst.h = 16;
				//dst.w = 16;
				//for (Node *node : *shortestPath) {
				//	dst.x = node->GetX() * 16;
				//	dst.y = node->GetY() * 16;
				//	SDL_RenderCopy(gfxData.renderer, resources.pathMarker.getTexture(), NULL, &dst);
				//}
				//for (auto pathNode : *shortestPath) {
				//	delete pathNode;
				//}
				//shortestPath->clear();


				// Render the changes above
				SDL_RenderPresent(gfxData.renderer);
			}
		}

		// These objects point to nodes that will be deleted in the maze object.
		source = nullptr;
		target = nullptr;
		player = nullptr;

		// Delete all the game objects (player, ghosts, etc)
		for (auto gameObject : gameObjects) {
			delete gameObject;
		}
		gameObjects.clear();


		ShutdownGFX(gfxData);
	}

	return 0;
}

bool InitResources(GFXData &gfxData, Resources &resources)
{
	/*** Textures ***/
	if (!(resources.wall.LoadFromFile(gfxData, "wall.png")))
		return false;

	if (!(resources.pathMarker.LoadFromFile(gfxData, "path_marker.png")))
		return false;

	if (!(resources.spriteSheet.LoadFromFile(gfxData, "pacman-sprites.png")))
		return false;

	if (!(resources.pill.LoadFromFile(gfxData, "pill.png")))
		return false;

	return true;
}


NodeList* FindShortestPath(Maze &maze, Node *start, Node *target)
{
	NodeList openList;
	NodeList closedList;

	start->SetG(0);
	start->SetF(start->GetG() + Heuristic(start, target));
	//start->parent = nullptr;
	openList.push_front(start);

	while (!openList.empty()) {
		// TODO This implies that the node at the beginning of the list
		// is the one with the lowest f-cost.
		Node *current = LowestOpenFCostNode(openList);
		if (current->GetX() == target->GetX() && current->GetY() == target->GetY()) {
			return ConstructPath(current);
		}

		openList.remove(current);
		closedList.push_back(current);

		for (Node *neighbour : GetNeighbours(current, maze))
		{
			if (FindNode(closedList, neighbour) == false) { // Check if neighbour is in closed list
				neighbour->SetF(neighbour->GetG() + Heuristic(neighbour, target));
				if (FindNode(openList, neighbour) == false) { // Check if neighbour is in open list
					openList.push_back(neighbour);
				}
				else {
					Node* openNeighbour = GetNode(openList, neighbour);
					if (neighbour->GetG() < openNeighbour->GetG()) {
						openNeighbour->SetG(neighbour->GetG());
						openNeighbour->SetParent(neighbour->GetParent());
					}
				}
			}
		}
	}

	NodeList* empty = new NodeList();
	return empty;
}

Node* LowestOpenFCostNode(NodeList openList)
{
	Node* lowestFCostNode = openList.front();

	for (Node* node : openList) {
		if (node->GetF() < lowestFCostNode->GetF()) {
			lowestFCostNode = node;
		}
	}

	return lowestFCostNode;
}

Node* GetNode(NodeList list, Node* node)
{
	for (Node *listNode : list) {
		if (listNode->GetX() == node->GetX() && listNode->GetY() == node->GetY()) {
			return listNode;
		}
	}

	Node* n = nullptr;
	return n;
}

bool FindNode(NodeList list, Node* node)
{
	for (Node *listNode : list) {
		if (listNode->GetX() == node->GetX() && listNode->GetY() == node->GetY()) {
			return true;
		}
	}

	return false;
}

int Heuristic(Node *start, Node *target)
{
	return abs(start->GetX() - target->GetX()) + abs(start->GetY() - target->GetY());
}

NodeList* ConstructPath(Node* node)
{
	NodeList *shortestPath = new NodeList();

	while (node->GetParent() != nullptr)
	{
		shortestPath->push_front(node);
		node = node->GetParent();
	}

	return shortestPath;
}

NodeList GetNeighbours(Node* current, Maze& maze)
{
	// TODO Try and get ghosts to find the shortest path through the exit portals
	NodeList neighbours;

	for (int x = -1; x <= 1; x += 2) {
		int y = 0;
		//if (current->GetX() + x >= 0 && current->GetX() + x < MAZE_WIDTH && current->GetY() + y >= 0 && current->GetY() + y < MAZE_HEIGHT) {
			Node* mazeNode = maze.GetNode(current->GetX() + x, current->GetY() + y);
			if (mazeNode != nullptr) {
				if (mazeNode->IsWalkable() || mazeNode->IsGhostDoor()) {
					Node* neighbourNode = new Node(*mazeNode); // Copy constructor
					neighbours.push_back(neighbourNode);
				}
			}
		//}
	}

	for (int y = -1; y <= 1; y += 2) {
		int x = 0;
		//if (current->GetX() + x >= 0 && current->GetX() + x < MAZE_WIDTH && current->GetY() + y >= 0 && current->GetY() + y < MAZE_HEIGHT) {
			Node* mazeNode = maze.GetNode(current->GetX() + x, current->GetY() + y);
			if (mazeNode != nullptr) {
				if (mazeNode->IsWalkable() || mazeNode->IsGhostDoor()) {
					Node* neighbourNode = new Node(*mazeNode); // Copy constructor
					neighbours.push_back(neighbourNode);
				}
			}
		//}
	}

	for (Node* node : neighbours) {
		// TODO Set cost to a constant (for grid)
		// TODO Set up for diagonal costs also
		node->SetG(current->GetG() + 1); // Cost is 1
		node->SetParent(current);
	}

	return neighbours;
}