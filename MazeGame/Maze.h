#pragma once

#include <vector>
#include <SDL.h>
#include "Globals.h"
#include "Node.h"

class Maze
{
public:
	Maze(SDL_Renderer *r);
	~Maze();
	void Render(SDL_Texture *texture);
	Node* GetNode(int x, int y);

private:
	std::vector<Node *> maze;
	SDL_Rect src = { 0, 208, 16, 16 };
	SDL_Rect dst = { 0, 0, 16, 16 };

	// 0: Walkable path, 1: Wall, 2: Power pill, 3: Ghost door.
	// Rotate map 90 degree clockwise for on-screen representation.
	int grid[MAZE_WIDTH][MAZE_HEIGHT] = { { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 },
	{ 1,0,0,2,0,0,0,0,0,1,1,1,1,1,0,1,1,1,1,1,0,0,0,0,1,1,0,2,0,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,0,0,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1 },
	{ 1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1 },
	{ 1,0,1,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,0,1,4,4,4,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1 },
	{ 1,0,0,0,0,0,1,1,0,0,0,0,1,4,4,4,1,0,1,1,0,0,0,0,1,1,0,0,0,0,1 },
	{ 1,1,1,1,1,0,1,1,1,1,1,0,3,4,4,4,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1 },
	{ 1,1,1,1,1,0,1,1,1,1,1,0,3,4,4,4,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1 },
	{ 1,0,0,0,0,0,1,1,0,0,0,0,1,4,4,4,1,0,1,1,0,0,0,0,1,1,0,0,0,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,0,1,4,4,4,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1,1,0,1 },
	{ 1,0,1,1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,1,1,1,0,1 },
	{ 1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,0,0,0,1,1,0,1 },
	{ 1,0,1,1,1,0,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,1,0,1,1,0,1,1,0,1 },
	{ 1,0,0,2,0,0,0,0,0,1,1,1,1,1,0,1,1,1,1,1,0,0,0,0,1,1,0,2,0,0,1 },
	{ 1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 } };


	SDL_Renderer* renderer;
};

