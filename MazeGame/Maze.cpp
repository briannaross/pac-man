#include "Maze.h"
#include <iostream>

Maze::Maze(SDL_Renderer *r)
{
	renderer = r;

	for (int x = 0; x < MAZE_WIDTH; x++) {
		for (int y = 0; y < MAZE_HEIGHT; y++) {
			bool walkable;
			bool ghostDoor;
			bool ghostSpawnPoint;

			// Check for walkable cells (includes power pill locations)
			if (grid[x][y] == 0 || grid[x][y] == 2 || grid[x][y] == 4)
				walkable = true;
			else
				walkable = false;

			if (grid[x][y] == 3)
				ghostDoor = true;
			else
				ghostDoor = false;

			if (grid[x][y] == 4)
				ghostSpawnPoint = true;
			else
				ghostSpawnPoint = false;

			Node *n = new Node(0, 0, x, y, walkable, ghostDoor, ghostSpawnPoint);

			maze.push_back(n);
		}
	}

	// Source and target sprite dimensions
	src.h = 8;
	src.w = 8;

	dst.h = 16;
	dst.w = 16;
}

Maze::~Maze()
{
	std::cout << "Maze deconstructor called" << std::endl;
	for (Node* cell : maze) {
		delete cell;
	}
}

void Maze::Render(SDL_Texture *texture)
{
	for (auto *cell : maze) {
		src.x = cell->GetX() * 8;
		src.y = cell->GetY() * 8;
		dst.x = cell->GetX() * 16;
		dst.y = cell->GetY() * 16;
		SDL_RenderCopy(renderer, texture, &src, &dst);

	}
}

Node* Maze::GetNode(int x, int y)
{
	for (auto cell : maze) {
		if (cell->GetX() == x && cell->GetY() == y) {
			return cell;
		}
	}

	return nullptr;
}
