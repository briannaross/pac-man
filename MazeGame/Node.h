#pragma once
#include <iostream>

class Node {

	friend std::ostream& operator<<(std::ostream& os, const Node& n);

public:
	// Constructor
	Node(int f, int g, int x, int y, bool walkable, bool ghostDoor, bool ghostSpawnPoint);

	// Copy constructor
	Node(const Node& source);

	// Overloaded assignment
	Node& operator=(const Node& source);

	// Destructor
	~Node();

	void SetF(int f);
	void SetG(int g);
	void SetX(int x);
	void SetY(int y);
	void SetParent(Node* parent);
	int GetF();
	int GetG();
	int GetX();
	int GetY();
	bool IsWalkable();
	bool IsGhostDoor();
	bool IsGhostSpawnPoint();
	Node* GetParent();

private:

	// Used for pathfinding
	int f; // Total estimated distance to target
	int g; // Distance from start

	// Coords of node
	int x;
	int y;

	// Can player and/or enemy traverse this node
	bool walkable;

	// Two tiles on map are traversable by ghosts
	bool ghostDoor;

	// The cell in the centre of the map where the ghosts spawn
	bool ghostSpawnPoint;

	// Reference to parent (used for pathfinding)
	Node* parent;

};

