#include "Node.h"

Node::Node(int f, int g, int x, int y, bool walkable, bool ghostDoor, bool ghostSpawnPoint)
{
	this->f = f;
	this->g = g;
	this->x = x;
	this->y = y;
	this->walkable = walkable;
	this->ghostDoor = ghostDoor;
	this->ghostSpawnPoint = ghostSpawnPoint;
}

Node::Node(const Node& source)
{
	this->f = source.f;
	this->g = source.g;
	this->x = source.x;
	this->y = source.y;
	this->walkable = source.walkable;
	this->ghostDoor = source.ghostDoor;
	this->ghostSpawnPoint = source.ghostSpawnPoint;
	this->parent = nullptr; // Should this be the same address as in source's parent?
}

Node& Node::operator=(const Node& source)
{
	//Check for self-assignment
	if (this == &source) {
		return *this;
	}

	this->f = source.f;
	this->g = source.g;
	this->x = source.x;
	this->y = source.y;
	this->walkable = source.walkable;
	this->ghostDoor = source.ghostDoor;
	this->ghostSpawnPoint = source.ghostSpawnPoint;
	this->parent = nullptr; // Do I need this line?

	return *this;
}

Node::~Node()
{
	//std::cout << "Destructor called for " << this->GetX() << ":" << this->GetY() << " @ memory location " << this << std::endl;
}

void Node::SetF(int f)
{
	this->f = f;
}

void Node::SetG(int g)
{
	this->g = g;
}

void Node::SetX(int x)
{
	this->x = x;
}

void Node::SetY(int y)
{
	this->y = y;
}

void Node::SetParent(Node* parent)
{
	this->parent = parent;
}

int Node::GetF()
{
	return f;
}

int Node::GetG()
{
	return g;
}

int Node::GetX()
{
	return x;
}

int Node::GetY()
{
	return y;
}

bool Node::IsWalkable()
{
	return walkable;
}

bool Node::IsGhostDoor()
{
	return ghostDoor;
}

bool Node::IsGhostSpawnPoint()
{
	return ghostSpawnPoint;
}

Node* Node::GetParent()
{
	return parent;
}

std::ostream& operator<<(std::ostream& os, const Node& n)
{
	os << "Node " << n.x << ":" << n.y << " has an F-value of " << n.f << " and a G-value of " << n.g << std::endl;
	os << "It is " << ((n.walkable) ? "" : "not") << " walkable and is " << ((n.ghostDoor) ? "" : "not") << " a ghost door.";

	return os << std::endl;
}