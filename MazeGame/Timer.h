#pragma once

class Timer
{
public:
	Timer(int tps);
	void SetTicksPerSecond(int tps);
	void Update();
	bool HasTicked();
	void BeginCountdown(int seconds);
	bool CountdownEnded();

private:
	int prevTime;
	int currentTime;
	int countdownSeconds;
	int countdownStart;
	float deltaTime;
	float frameTime;
	float frameInterval;
	float TicksPerSecond;
};
