#pragma once
#include <SDL.h>
#include <iostream>

class GameObject
{
public:
	GameObject(SDL_Texture *texture);
	~GameObject();
	void SetPosition(int x, int y);
	int GetX();
	int GetY();
	void Render(SDL_Texture* t);
	static void SetRenderer(SDL_Renderer* r);
	virtual void WhereTheHellAmI() = 0;
	virtual void Reset() = 0;

protected:
	int x;
	int y;
	bool lerping;
	SDL_Rect dst;
	SDL_Rect src;
	SDL_Texture* texture;
	SDL_Point vector;
	static SDL_Renderer *renderer;
};

