#pragma once
#include <SDL.h>
#include "GameObject.h"
#include "Maze.h"
#include "Timer.h"
#include "Globals.h"


class Player : public GameObject
{
public:
	Player(SDL_Texture* texture);
	~Player();
	void MoveUp(Maze &maze);
	void MoveDown(Maze &maze);
	void MoveLeft(Maze &maze);
	void MoveRight(Maze &maze);
	void Animate();
	bool EatPill(SDL_Point &pillLoc);

	void WhereTheHellAmI();
	void Reset() {}


private:
	void Move(SDL_Point &vector);
	Timer *timer;
	int animFrame;
	int score;
};

